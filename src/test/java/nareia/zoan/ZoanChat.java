package nareia.zoan;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.FindBy;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;

//@RunWith(ConcurrentTestRunner.class)
public class ZoanChat {

	private WebDriver driver;
	
	@FindBy(id = "message")
    private WebElement messageBox;
	
	public ZoanChat() {
		// TODO Auto-generated constructor stub
	}
	
//	@Test
//    public void shouldRunInParallel1() {
//        System.out.println("I'm running on thread " + Thread.currentThread().getName());
//    }
//
//    @Test
//    public void shouldRunInParallel2() {
//        System.out.println("I'm running on thread " + Thread.currentThread().getName());
//    }
//
//    @Test
//    public void shouldRunInParallel3() {
//        System.out.println("hola " + Thread.currentThread().getName());
//    }
	
	//edge, safari, firefox, chrome
    
	@Test
	public void testRamira() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://zoanwebinar.azurewebsites.net");
		String MainWindow=driver.getWindowHandle();		
		driver.switchTo().alert().sendKeys("Fernando Cristino");
		driver.switchTo().alert().accept();
		driver.switchTo().window(MainWindow);
		Thread.sleep(20000);
		WebElement hola  = driver.findElement(By.id("message"));
		for(int i=0; i<700; i++) {
			
			if (i % 2 == 0)
				hola.sendKeys("A veces gano" + Keys.ENTER);
			else
				hola.sendKeys("A veces no" + Keys.ENTER);
			//driver.findElement(By.id("message")).sendKeys(Keys.ENTER);
		}
//		WebElement s
		driver.quit();

	}
		
	@Test
	public void testKermen() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.get("https://zoanwebinar.azurewebsites.net");

		String MainWindow=driver.getWindowHandle();
		
		driver.switchTo().alert().sendKeys("Kermen");
		driver.switchTo().alert().accept();
		driver.switchTo().window(MainWindow);
		Thread.sleep(20000);
		WebElement hola  = driver.findElement(By.id("message"));
		for(int i=0; i<700; i++) {
			if (i % 2 == 0) {
				hola.sendKeys("Tanto va el cantaro a la fuente" + Keys.ENTER);
				hola.sendKeys(Keys.ENTER);
			}
			else {
				hola.sendKeys("Tanto va el cantaro a la fuente" + Keys.ENTER);
				hola.sendKeys(Keys.ENTER);
			}
//driver.findElement(By.id("message")).sendKeys(Keys.ENTER);
		}
//		WebElement s
		driver.quit();

	}
	
	@Test
	public void testRaul() throws InterruptedException {
		System.setProperty("webdriver.safari.driver","src/test/resources/drivers/safaridriver");
		WebDriver driver = new SafariDriver();
		driver.get("https://zoanwebinar.azurewebsites.net");
		String MainWindow=driver.getWindowHandle();		
		driver.switchTo().alert().sendKeys("El parva Bermudez");
		driver.switchTo().alert().accept();
		driver.switchTo().window(MainWindow);
		Thread.sleep(3000);
		WebElement hola  = driver.findElement(By.id("message"));
		for(int i=0; i<700; i++) {
			if (i % 2 == 0)
				hola.sendKeys("Elin" + Keys.ENTER);
			else
				hola.sendKeys("Adaptado" + Keys.ENTER);
//			hola.sendKeys("Raul el diariero" + Keys.ENTER);
			//driver.findElement(By.id("message")).sendKeys(Keys.ENTER);
		}
//		WebElement s
		driver.quit();

	}
//	
//	@AfterMethod
//    public void clean() {
//    	driver.quit();
//    }
//	
//	@Test
//	public void testCarlos() throws InterruptedException {
//		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
//		driver = new ChromeDriver();
//		driver.get("https://zoanwebinar.azurewebsites.net");
//		String MainWindow=driver.getWindowHandle();		
//		driver.switchTo().alert().sendKeys("Carlos");
//		driver.switchTo().alert().accept();
//		driver.switchTo().window(MainWindow);
//		Thread.sleep(3000);
//		WebElement hola  = driver.findElement(By.id("message"));
//		for(int i=0; i<700; i++) {
//			hola.sendKeys("Carlitoos al habla" + Keys.ENTER);
//			//driver.findElement(By.id("message")).sendKeys(Keys.ENTER);
//		}
////		WebElement s
//	}
//	
//	@Test
//	public void testCarmen() throws InterruptedException {
//		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
//		driver = new ChromeDriver();
//		driver.get("https://zoanwebinar.azurewebsites.net");
//		String MainWindow=driver.getWindowHandle();		
//		driver.switchTo().alert().sendKeys("Carmen");
//		driver.switchTo().alert().accept();
//		driver.switchTo().window(MainWindow);
//		Thread.sleep(3000);
//		WebElement hola  = driver.findElement(By.id("message"));
//		for(int i=0; i<700; i++) {
//			hola.sendKeys("Hola soy Carmen" + Keys.ENTER);
//			//driver.findElement(By.id("message")).sendKeys(Keys.ENTER);
//		}
////		WebElement s
//	}

	
	
//	xattr -d com.apple.quarantine <name-of-executable>
}
