package nareia.zoan;

import java.io.File;

import org.apache.commons.io.FileUtils;

//import org.apache.tools.ant.util.FileUtils;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.Test;

import org.testng.annotations.DataProvider;

public class UiScreenshotChat {

	private WebDriver driver;

	public UiScreenshotChat() {
		// TODO Auto-generated constructor stub
	}
	
	@DataProvider(name = "browser")
	public static Object[][] browser() {
	    return new Object[][] {
	    	{"webdriver.chrome.driver", "src/test/resources/drivers/chromedriver", "chrome"},
	    	{"webdriver.gecko.driver", "src/test/resources/drivers/geckodriver", "firefox"},
	    	{"webdriver.safari.driver", "src/test/resources/drivers/safaridriver", "safari"},
	    	{"webdriver.edge.driver", "src/test/resources/drivers/msedgedriver", "edge"}
	    };
	} 
	@Test(dataProvider = "browser")
	public void browserScreenshot(String key, String value, String browserName) throws Exception {
		
		System.setProperty(key, value);
		if(browserName.equalsIgnoreCase("chrome")) {
			driver = new ChromeDriver();
		}
		else if(browserName.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		}
		else if(browserName.equalsIgnoreCase("safari")) 
			driver = new SafariDriver();
		else
			driver = new EdgeDriver();

		driver.manage().window().maximize();		
		driver.get("https://zoanwebinar-frontend.azurewebsites.net/");
		Thread.sleep(5000);
		this.takeSnapShot(driver, "/Users/bzerpa/Desktop/"+ browserName +".png");
		driver.quit();
	}

	public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
        TakesScreenshot scrShot =((TakesScreenshot)webdriver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile=new File(fileWithPath);
        FileUtils.copyFile(SrcFile, DestFile);

    }

}
